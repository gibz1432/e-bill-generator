const User = require('../models/User');

//get Users
exports.getUsers = async(req,res) =>{
    try{
        const users = await User.find({});
        if(!users) return res.send('No users yet');
    
        return res.send(users)
    }
    catch (err){
        if(err) return res.send(err)
    }
}

// add user
exports.addUser = async(req,res) => {
    try{
        let newUser = new User({
            name: req.body.name,
            email: req.body.email,
        })
    
        newUser = await newUser.save()
        return res.send(`${newUser.name} has been added`)
    }catch(err){
        res.send(err.message)
    }
}

//delete user
exports.deleteUser = async(req, res) => {

    try{
        // const user = await User.deleteOne({})
        const user = await User.findByIdAndRemove(req.params.userId);
        return res.send(`${user.name} has been removed`)
    }catch(err){
        res.send(err.message)
    }
}

// update device
exports.updateUser = async(req, res) => {
    try{
        const user = await User.findById(req.params.userId);
        
        user.name = req.body.name
        user.email = req.body.email

        await user.save()

        res.send(user);

    }catch(err){
        res.send(err.message)
    }
}