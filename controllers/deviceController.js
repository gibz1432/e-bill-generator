const Device = require('../models/Device');
const User = require('../models/User')

//get Devices
exports.getDevices = async(req,res) =>{
    try{
        const devices = await Device.find({});
        if(!devices) return res.send('No devices yet');
    
        return res.send(devices)
    }
    catch (err){
        if(err) return res.send(err)
    }
}

//get user devices
exports.getUserDevices = async(req,res) =>{
    try{
        const devices = await Device.findOne({"user.userId": req.params.userId});
        if(!devices) return res.send('No devices yet');
    
        return res.send(devices)
    }
    catch (err){
        if(err) return res.send(err)
    }
}

// add user
exports.addDevice = async(req,res) => {
    try{
        const user = await User.findById(req.params.userId)
        const device = await Device.findOne({"user.userId": req.params.userId});

        if(!device){
            let addDevice = new Device({
                user: {
                    userId: user._id,
                }
            })
    
                addDevice.devices.push({
                    name: req.body.name,
                    wattage: req.body.wattage,
                    dailyUsageHours: req.body.dailyUsageHours,
                })
    
             let addedDevice = await addDevice.save();
    
             return res.send(addedDevice);
        }else{
            device.devices.push({
                name: req.body.name,
                wattage: req.body.wattage,
                dailyUsageHours: req.body.dailyUsageHours,
            })

            let newDevice = await device.save();

            return res.send(newDevice);
        }

    }catch(err){
        res.send(err.message)
    }
}

// delete device
exports.deleteDevice = async(req, res) => {
    try{
        const deviceData = await Device.findOne({"user.userId": req.params.userId});
        const devices = deviceData.devices
        const indexToDelete = devices.findIndex(obj => obj.id === req.body.deviceId);

        if (indexToDelete !== -1) {
            devices.splice(indexToDelete, 1);
        }

        await deviceData.save()

        return res.send(`Device has been successfully removed`)
    }catch(err){
        res.send(err.message)
    }
}


// update device
exports.updateDevice = async(req, res) => {
    try{
        const deviceData = await Device.findOne({"user.userId": req.params.userId});
        const devices = deviceData.devices
        // Find the index of the object you want to update
        const indexToUpdate = devices.findIndex(obj => obj.id === req.body.deviceId);

        if (indexToUpdate !== -1) {
            // Update the object at the found index
            devices[indexToUpdate].name = req.body.name;
            devices[indexToUpdate].wattage = req.body.wattage;
            devices[indexToUpdate].dailyUsageHours = req.body.dailyUsageHours;
        } 

        await deviceData.save()

        res.send(devices);

    }catch(err){
        res.send(err.message)
    }
}