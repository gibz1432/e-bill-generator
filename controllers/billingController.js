const Device = require('../models/Device');
const User = require('../models/User')
const Billing = require('../models/Billing')
const nodemailer = require('nodemailer')
const puppeteer = require('puppeteer');
const { JSDOM } = require('jsdom');

//get Devices
exports.getBillings = async(req,res) =>{
    try{
        const billings = await Billing.find({});
        if(!billings) return res.send('No billing yet');
    
        return res.send(billings)
    }
    catch (err){
        if(err) return res.send(err)
    }
}

// add billing
exports.computeBill = async(req,res) => {
    try{
        const users = await User.find({});
        const devices = await Device.find({});

        // Use reduce() to calculate the total billing
        const totalWattage = devices.reduce((total, item) => {
            return total + item.devices.reduce((itemTotal, value) => itemTotal + value.wattage*value.dailyUsageHours*30, 0);
        }, 0);
    

        const prevReading = await Billing.findOne().sort('-_id')


        if(req.body.currentReading <= prevReading.currentReading){
            return res.status(400).send('Current reading should be greater then the previous reading!');
        }

        let newBilling = new Billing({
            previousReading: prevReading.currentReading || 4900,
            currentReading: req.body.currentReading,
            pricePerKW: req.body.pricePerKW,
            month: req.body.month,
            year: req.body.year,
            bill: (req.body.currentReading - prevReading.currentReading || 4900)*req.body.pricePerKW
        })

        // Create a PDF document
        // HTML content with your table
        const table1 = `
        <!DOCTYPE html>
            <html>
                <head>
                    <style>
                    table {
                        border-collapse: collapse;
                        width: 100%;
                    }
                    th, td {
                        border: 1px solid black;
                        padding: 8px;
                        text-align: center;
                    }
                    th {
                        background-color: lightgray; /* Add background color to table headers */
                    }
                    </style>
                </head>
                <body>
                    <h2 style="text-align: center;">Billing Summary for the month of ${req.body.month} ${req.body.year}</h2>
                    <table>
                    <tr>
                        <th>Previous Reading</th>
                        <th>Current Reading</th>
                        <th>Consumption (kW)</th>
                        <th>Computed Consumption (W)</th>
                        <th>Bill (Php)</th>
                    </tr>
                    <tr>
                        <td>${prevReading.currentReading}</td>
                        <td>${req.body.currentReading}</td>
                        <td>${req.body.currentReading - prevReading.currentReading}</td>
                        <td>${totalWattage}</td>
                        <td>${(req.body.currentReading - prevReading.currentReading)*req.body.pricePerKW}</td>
                    </tr>
                    </table>
                </body>
            </html>
        `;

      // Create a virtual DOM environment
        const dom = new JSDOM(`<!DOCTYPE html><html><body></body></html>`);

        // Access the document
        const document = dom.window.document;

        // Create a table element
        const table = document.createElement('table');
        table.style.border = '1px solid #000'; // Add a border to the table
        table.style.marginTop = '30px';

        // Create a table header row
        const headerRow = table.insertRow();
        const headers = ['Name', 'Consumption (W)', 'Amount Due (Php)'];

        headers.forEach(headerText => {
        const th = document.createElement('th');
        th.textContent = headerText;
        th.style.padding = '8px'; // Add padding to header cells
        th.style.border = '1px solid #000'; // Add a border to header cells
        headerRow.appendChild(th);
});

        // Create table rows and cells
        for (let i = 0; i < users.length; i++) {

            const device = await Device.findOne({"user.userId": users[i]._id});
            const individualWattage = device.devices.reduce((total, item) => total + item.wattage*item.dailyUsageHours*30, 0);
            const bill = (req.body.currentReading - prevReading.currentReading)*req.body.pricePerKW;
            const individualBill = (individualWattage/totalWattage)*bill;

        const row = table.insertRow();
        for (let j = 0; j < 3; j++) {
            const cell = row.insertCell();
            // cell.textContent = `Row ${i + 1}, Cell ${j + 1}`;
            if(j == 0){
                cell.textContent = users[i].name
            }
            if(j == 1){
                cell.textContent = individualWattage
            }
            if(j == 2){
                cell.textContent = Math.round(individualBill)
            }
            cell.style.padding = '8px'; // Add padding to cells
            cell.style.border = '1px solid #000'; // Add a border to cells
        }
        row.style.backgroundColor = i % 2 === 0 ? '#eee' : '#fff'; // Alternate row background color
        }

        // Set table width
        table.style.width = '100%';

        // Append the table to the document
        document.body.appendChild(table);

        // Store the modified HTML in a variable
        const table2 = document.documentElement.outerHTML;


        const htmlContent = table1 + table2
  

        // Function to convert HTML to PDF
        async function htmlToPdf(htmlContent) {
            const browser = await puppeteer.launch({
                args: [
                  "--disable-setuid-sandbox",
                  "--no-sandbox",
                  "--single-process",
                  "--no-zygote",
                ],
                executablePath: '/usr/bin/google-chrome-stable'
              });
            const page = await browser.newPage();
        
            // Set the content and format of the page
            await page.setContent(htmlContent);
            const pdfBuffer = await page.pdf({ format: 'A4' });
        
            await browser.close();
        
            return pdfBuffer;
        }
        

                    
        

        // send email
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            secure: false,
            auth: {
                user: 'gibotajada@gmail.com',
                pass: 'jose wbvv urqi uoym'
            }
        })

        for(let i = 0; i < users.length; i++){
            const device = await Device.findOne({"user.userId": users[i]._id});
            const individualWattage = device.devices.reduce((total, item) => total + item.wattage*item.dailyUsageHours*30, 0);
            const bill = (req.body.currentReading - prevReading.currentReading)*req.body.pricePerKW;
            const individualBill = ((individualWattage*30)/totalWattage)*bill;

            const sendMail = async(pdfBuffer) => {
                transporter.sendMail({
                    from: 'Sender Name gibotajada@gmail.com',
                    to: users[i].email,
                    subject: 'Monthly Electric Bill',
                    text: `See attached pdf file of your billing for ${req.body.month}.\ If you have any questions, just approach the Admin`,
                    attachments: [
                        {
                            filename: 'Billing.pdf',
                            content: pdfBuffer,
                        },
                    ],
                }, function(error, info){
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('Email sent: ' + info.response);
                    }
                });
            }
            // Usage
            (async () => {
                try {
                const pdfBuffer = await htmlToPdf(htmlContent);
                await sendMail(pdfBuffer);
                } catch (error) {
                console.error('Error:', error);
                }
            })();

        }
 
        let newAddedBilling = await newBilling.save();

        res.send(newAddedBilling);


    }catch(err){
        res.send(err.message)
    }
}


//get latest billing
exports.getLatestBilling = async(req,res) =>{
    try{
        const latestBilling = await Billing.findOne().sort('-_id')
        return res.send(latestBilling)
    }
    catch (err){
        if(err) return res.send(err)
    }
}

