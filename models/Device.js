const mongoose = require('mongoose');


const deviceSchema = new mongoose.Schema({
  user: { 
    type: new mongoose.Schema({
        userId : {
          type:String,
          required: [true, "firstname name is required"]
        },
    }),
    required: true
  },
  devices: [{
    type: new mongoose.Schema({
        name: {
        type: String,
        required: true,
      },
        wattage: {
        type: Number,
        required: true
      },
        dailyUsageHours:{
          type: Number,
          required: true
        }
    }),
  }],
});

module.exports = mongoose.model('Device', deviceSchema);

