const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({
  name : {
    type:String,
    required: [true, "Name is required"],
    uppercase: true
  }, 
  email: {
    type: String,
    required: [true, "Email is required"],
    lowercase: true
  },
  billings: [{
    type: new mongoose.Schema({
        month: {
        type: String,
      },
        year: {
        type: Number,
      },
        totalWattage: {
        type: Number,
        },
        amountDue:{
          type: Number,
        }
    }),
  }],

})

module.exports = mongoose.model('User', userSchema);
