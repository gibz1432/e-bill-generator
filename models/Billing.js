const mongoose = require('mongoose');

const billingSchema = new mongoose.Schema({
    previousReading : {
        type: Number,
        required: [true, "Previous reading is required"],
    },
    currentReading : {
        type: Number,
        required: [true, "Current reading is required"],
    },
    pricePerKW : {
        type: Number,
        required: [true, "Price per kw is required"],
    },
    month: {
        type: String,
        required: [true, 'Month is required']
    },
    year: {
        type: String,
        required: [true, 'Year is required']
    },
    bill: {
        type: Number,
    },
    createdOn: {
        type: Date,
        default: new Date()
        }
});

module.exports = mongoose.model('Billing', billingSchema);
