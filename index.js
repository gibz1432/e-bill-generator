const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv');
const userRoutes = require('./routes/userRoutes')
const deviceRoutes = require('./routes/deviceRoutes')
const billingRoutes = require('./routes/billingRoutes')



const app = express();
const port = 4000;
const secret = 'mongodb+srv://orfenoko1:malnour1234@gibz.8x8pf.mongodb.net/Pelayo_Billing?retryWrites=true&w=majority'
dotenv.config();

//Database Connect
mongoose.connect(secret, 
    {
        useNewUrlParser : true,
        useUnifiedTopology : true
    }
);

    let db = mongoose.connection;
    
    db.on('error', console.error.bind(console, "Connection Error"));
    db.once('open', () => console.log("Successfully connected to MongoDB"));


app.get('/', (req, res) => {
    res.send('Hello World');
})

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.use('/users', userRoutes);
app.use('/devices', deviceRoutes);
app.use('/billings',billingRoutes);


app.listen(port, () =>  console.log(`Server running at port ${port}`));