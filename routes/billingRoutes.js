const express = require('express');
const router = express.Router();
const billingController = require('../controllers/billingController');

// add users
router.get('/getLatestBilling',billingController.getLatestBilling);
router.get('/getBillings',billingController.getBillings);
router.post('/computeBill', billingController.computeBill);

// router.delete('/deleteUser/:userId', userController.deleteUser);

module.exports = router;