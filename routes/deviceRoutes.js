const express = require('express');
const router = express.Router();
const deviceController = require('../controllers/deviceController');

// add users
router.get('/getDevices',deviceController.getDevices);
router.post('/addDevice/:userId', deviceController.addDevice);
router.get('/getUserDevices/:userId', deviceController.getUserDevices);
router.put('/deleteDevice/:userId', deviceController.deleteDevice);
router.put('/updateDevice/:userId', deviceController.updateDevice);
// router.delete('/deleteUser/:userId', userController.deleteUser);

module.exports = router;