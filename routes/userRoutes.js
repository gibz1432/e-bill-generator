const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');

// add users
router.get('/getUsers',userController.getUsers);
router.post('/addUser', userController.addUser);
router.delete('/deleteUser/:userId', userController.deleteUser);
router.put('/updateUser/:userId', userController.updateUser);

module.exports = router;